/********************************************************************************
** Form generated from reading UI file 'opencryptoprice.ui'
**
** Created by: Qt User Interface Compiler version 5.15.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_OPENCRYPTOPRICE_H
#define UI_OPENCRYPTOPRICE_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_OpenCryptoPrice
{
public:
    QWidget *centralWidget;
    QLabel *label;
    QToolButton *toolButton;
    QPushButton *pushButton;
    QLabel *label_2;

    void setupUi(QMainWindow *OpenCryptoPrice)
    {
        if (OpenCryptoPrice->objectName().isEmpty())
            OpenCryptoPrice->setObjectName(QString::fromUtf8("OpenCryptoPrice"));
        OpenCryptoPrice->resize(512, 512);
        centralWidget = new QWidget(OpenCryptoPrice);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        label = new QLabel(centralWidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(170, 20, 170, 20));
        toolButton = new QToolButton(centralWidget);
        toolButton->setObjectName(QString::fromUtf8("toolButton"));
        toolButton->setGeometry(QRect(196, 140, 120, 40));
        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(196, 310, 120, 40));
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(176, 390, 160, 20));
        OpenCryptoPrice->setCentralWidget(centralWidget);

        retranslateUi(OpenCryptoPrice);

        QMetaObject::connectSlotsByName(OpenCryptoPrice);
    } // setupUi

    void retranslateUi(QMainWindow *OpenCryptoPrice)
    {
        OpenCryptoPrice->setWindowTitle(QCoreApplication::translate("OpenCryptoPrice", "OpenCryptoPrice", nullptr));
        label->setText(QCoreApplication::translate("OpenCryptoPrice", "Predict crypto-currency price", nullptr));
        toolButton->setText(QCoreApplication::translate("OpenCryptoPrice", "Select...", nullptr));
        pushButton->setText(QCoreApplication::translate("OpenCryptoPrice", "Predict!", nullptr));
        label_2->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class OpenCryptoPrice: public Ui_OpenCryptoPrice {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_OPENCRYPTOPRICE_H
