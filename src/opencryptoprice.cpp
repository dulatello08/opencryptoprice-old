#include "opencryptoprice.h"
#include "ui_opencryptoprice.h"
#include <QtWidgets/QToolButton>
OpenCryptoPrice::OpenCryptoPrice(QWidget *parent) : QMainWindow(parent), ui(new Ui::OpenCryptoPrice)
{
    ui->setupUi(this);
    QToolButton* spinner = ui->toolButton;
    QMenu* menu = new QMenu(this);
    spinner->setMenu(menu);
    spinner->setPopupMode(QToolButton::InstantPopup);
    QAction *bitcoin = new QAction("Bitcoin", this);
    menu->addAction(bitcoin);
    connect(bitcoin, &QAction::triggered, this, [this, spinner]{
        OpenCryptoPrice::bitcoin = true;
        spinner->setText("Bitcoin");
    });
    pushButton = ui->pushButton;
    label_2 = ui->label_2;
    connect(pushButton, &QPushButton::clicked, this, [this]() {
        label_2->setText("Error: Not implemented yet.");
    });
}

OpenCryptoPrice::~OpenCryptoPrice() = default;

