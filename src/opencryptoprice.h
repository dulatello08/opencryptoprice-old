#ifndef OPENCRYPTOPRICE_H
#define OPENCRYPTOPRICE_H

#include <QMainWindow>
#include <QScopedPointer>
#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QWidget>

namespace Ui {
class OpenCryptoPrice;
}

class OpenCryptoPrice : public QMainWindow
{
    Q_OBJECT
    public:
        explicit OpenCryptoPrice(QWidget *parent = nullptr);
        ~OpenCryptoPrice() override;
    private:
            bool bitcoin;
            QAction *actionPredict;
            QAction *actionComing_soon;
            QWidget *centralWidget;
            QLabel *label;
            QToolButton *spinner;
            QPushButton *pushButton;
            QLabel *label_2;
            QMenu *menuPredict;
    private:
        QScopedPointer<Ui::OpenCryptoPrice> ui;
};

#endif // OPENCRYPTOPRICE_H
